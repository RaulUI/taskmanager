var module = angular.module("myApp")

module.factory('addEmployeeService', ['$resource', function($resource){
	
	var addEmployee = $resource('http://localhost:8080/AddNewEmployee', {}, {
		'addJson' : {
			method: 'POST',
			isArray: false,
			headers: {
				'Content-Type': 'application/json'
			}
		}
	});
	
	return {
		'addEmployee': addEmployee
	};
	
}]);

module.factory('editEmployeeService', ['$resource', function($resource){
	
	var editEmployee = $resource('http://localhost:8080/EditEmployee', {}, {
		'addJson' : {
			method: 'POST',
			isArray: false,
			headers: {
				'Content-Type': 'application/json'
			}
		}
	});
	
	return {
		'editEmployee': editEmployee
	};
	
}]);

module.factory('assignEmployeesService', ['$resource', function($resource){
	
	var assignEmployees = $resource('http://localhost:8080/AssignEmployees', {}, {
		'addJson' : {
			method: 'POST',
			isArray: true,
			headers: {
				'Content-Type': 'application/json'
			}
		}
	});
	
	return {
		'assignEmployees': assignEmployees
	};
	
}]);

module.factory('assignTasksService', ['$resource', function($resource){
	
	var assignTasks = $resource('http://localhost:8080/AssignTasks', {}, {
		'addJson' : {
			method: 'POST',
			isArray: true,
			headers: {
				'Content-Type': 'application/json'
			}
		}
	});
	
	return {
		'assignTasks': assignTasks
	};
	
}]);

module.factory('addTaskService', ['$resource', function($resource){
	
	var addTask = $resource('http://localhost:8080/AddNewTask', {}, {
		'addJson' : {
			method: 'POST',
			isArray: false,
			headers: {
				'Content-Type': 'application/json'
			}
		}
	});
	
	return {
		'addTask': addTask
	};
	
}]);

module.factory('deleteServiceEmployee', ['$resource', function($resource){
	
	var deleteEmployee = $resource('http://localhost:8080/deleteEmployee', {}, {
		'deleteJson' : {
			method: 'POST',
			isArray: false,
			headers: {
				'Content-Type': 'application/json'
			}
		}
	});
	
	return {
		'deleteEmployee': deleteEmployee
	};
	
}]);

module.factory('deleteServiceTask', ['$resource', function($resource){
	
	var deleteTask = $resource('http://localhost:8080/deleteTask', {}, {
		'deleteJson' : {
			method: 'POST',
			isArray: false,
			headers: {
				'Content-Type': 'application/json'
			}
		}
	});
	
	return {
		'deleteTask': deleteTask
	};
	
}]);
