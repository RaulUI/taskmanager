var app = angular.module("myApp", ["ngRoute", "ngResource"]);
	  
app.config(function($routeProvider) {
    $routeProvider
    .when("/employees_list", {
        templateUrl : "views/employees_list.html",
        controller : 'EmployeesController'
    })
    .when("/tasks_list", {
        templateUrl : "views/tasks_list.html",
        controller : 'TasksController'
    })
    .when("/add_employee", {
        templateUrl : "views/add_employee.html",
        controller : 'AddEmployeeController'
    })
    .when("/my_tasks", {
        templateUrl : "views/my_tasks.html",
        controller : 'LoggedUserTasksController'
    })
    .when('/login', {
		templateUrl : 'login.html',
		controller : 'LoginController'
    })
    .otherwise({
        redirect: '/index'
    });
});

app.controller('notificationController', function ($scope, $rootScope) {
	$rootScope.notification = {
			title: "",
			text: "",
			type: ""
	};
	$("#notifModal").modal();
	$("#notifModal").hide();
});

app.controller('PageTitleController', function ($scope, $http, $rootScope, $location, $routeParams){
	$rootScope.$on('$routeChangeSuccess', function(e, current, pre) {
		
		//Prepare title of the page
		$scope.pageTitle = $location.path();
		$scope.pageTitle = $scope.pageTitle.replace(/_/g, " ");
		$scope.pageTitle = $scope.pageTitle.replace("/", "");
		$scope.pageTitle = $scope.pageTitle.substring(0, 1).toUpperCase() + $scope.pageTitle.substring(1);
		var currentIndex = 0;
		
		while (currentIndex < $scope.pageTitle.length) {
			if ($scope.pageTitle.charAt(currentIndex) == " ") {
				$scope.pageTitle = $scope.pageTitle.substring(0, currentIndex+1) + 
				$scope.pageTitle.substring(currentIndex+1, currentIndex+2).toUpperCase() + 
				$scope.pageTitle.substring(currentIndex+2);
			}
			currentIndex++;
		}
		
		$scope.pageTitleHidden = $scope.pageTitle;
	});
	
	//Set user name and email
	$http.get('/user').then(function(data) {
        $scope.loggedUser = data.data;
        if ($scope.loggedUser.name == "" || !$scope.loggedUser || angular.isUndefined($scope.loggedUser.name))
        	$scope.loggedUserName = "User";
        else {
        	$scope.loggedUserName = $scope.loggedUser.name;
        }
    });
	
	$scope.logout = function() {
		notification($scope, $timeout, $scope.loggedUser.name, "You logged out successfully.", "success");
	}
	
});

app.controller('LoginController', function ($scope, $http, $timeout){
	console.log("Now");
	$scope.login = function() {
		$http.get('/user').then(function(data) {
	        $scope.loggedUser = data.data;
	    	notification($scope, $timeout, "Hello " + $scope.loggedUser.name, "You logged in successfully.", "success");
	    });
	}
});

app.controller('EmployeesController', function ($scope, $http, deleteServiceEmployee, assignTasksService, editEmployeeService, $timeout){
	
	$scope.skills = ["None", "Beginner", "Intermediate", "Advanced", "Expert"];

	$http.get('/Tasks').then(function(data) {
        $scope.tasks = data.data;
    });
	
	$http.get('/Employees').then(function(data) {
        $scope.employees = data.data;
    });
	
	$scope.updateEmployee = function (employee) {
		notification($scope, $timeout, "Title", "ASDwefwef swdfwdfsdf vcvxv. wefrww3rADEd e", "info");
		$scope.selectedEmployee = employee;
	}
	
	$scope.assignTasks = function (employee) {
		$scope.selectedEmployee = employee;
		var alreadyExists = false;
		$scope.tasksList = [];
		
		//Check for duplicates and populate the list to show in task selector
		for (var i = 0; i < $scope.tasks.length; i++) {
			for (var j = 0; j < employee.tasks.length; j++) {
				if (employee.tasks[j].taskId == $scope.tasks[i].taskId) 
					alreadyExists = true;
			}
			//Conditions to find employees
			if (!alreadyExists && $scope.tasks[i].employeesNeeded.hasOwnProperty(employee.skill) && $scope.tasks[i].employeesThreshold > $scope.tasks[i].employeesActive) 
				$scope.tasksList.push($scope.tasks[i]);
			alreadyExists = false;
		}
		if (employee.tasksThreshold <= employee.tasks.length) {
			//Notif
			
			$scope.tasksList = [];
		}
		for (var i = 0; i < $scope.tasksList.length; i++) {
			$scope.tasksList[i].toAdd = false;
		}
	} 
	
	$scope.addTasksToEmployee = function () {
		var emptyTask = {'id':-1, 'name':""+$scope.selectedEmployee.id, 'employeesThreshold':0, 'description':"", 'employeesNeededCode': ""};
		var jsonObj = "";
		var jsonObjList = [];
		jsonObjList.push(emptyTask);
		for (var i = 0; i < $scope.tasksList.length; i++) {
			if ($scope.tasksList[i].toAdd == true) {
				jsonObjList[0].employeesThreshold++;
				jsonObjList.push($scope.tasksList[i]);
			}
		}
		jsonObj = JSON.stringify(jsonObjList);

		//Check if list isn't longer than tasksThreshold and after add it or not
		if ($scope.selectedEmployee.tasks.length + jsonObjList[0].employeesThreshold > $scope.selectedEmployee.tasksThreshold) {
			//Notif
			return;
		}
		assignTasksService.assignTasks.addJson({},
				jsonObj, function (response) {
			
		});
	}
	
	$scope.editEmployee = function () {
		var data = $scope.selectedEmployee;
		var jsonObj = JSON.stringify(data);
		
		editEmployeeService.editEmployee.addJson({},
				jsonObj, function (response) {
		});
	}
	
	$scope.deleteEmployee = function (employee) {
		
		var jsonObj = JSON.stringify(employee);
		console.log(jsonObj);
		var index = $scope.employees.indexOf(employee);
		$timeout( function(){
			$scope.employees.splice(index, 1);
		}, 500 );
		
		
		deleteServiceEmployee.deleteEmployee.deleteJson({},
				jsonObj, function (response) {
		});
	}
});

app.controller('TasksController', function ($scope, $http, deleteServiceTask, assignEmployeesService, $timeout){
	
	$scope.skills = ["None", "Beginner", "Intermediate", "Advanced", "Expert"];
	$scope.autoAssign = false;
	
	$http.get('/Tasks').then(function(data) {
        $scope.tasks = data.data;
    });
	
	$http.get('/Employees').then(function(data) {
        $scope.employees = data.data;
    });
	
	$scope.assignEmployees = function (task) {
		findEmployeesForTask(task, $scope, $scope.selectedTask);
	}
	
	//After add button is pressed
	$scope.addEmployeesToTask = function () {
		var emptyEmployee = {'id':-1, 'name':""+$scope.selectedTask.taskId, 'tasksThreshold':0, 'skill':0};
		var jsonObj = "";
		var jsonObjList = [];
		jsonObjList.push(emptyEmployee);
		for (var i = 0; i < $scope.employeeList.length; i++) {
			if ($scope.employeeList[i].toAdd == true) {
				jsonObjList[0].tasksThreshold = jsonObjList[0].tasksThreshold + 1;
				jsonObjList.push($scope.employeeList[i]);
				//Remove from employeesNeeded one skill that employee has
				
				$scope.selectedTask.employeesNeeded[$scope.employeeList[i].skill]--;
				if ($scope.selectedTask.employeesNeeded[$scope.employeeList[i].skill] < 0) {
					//NOTIF
					return;
				}
			}
		}
		jsonObj = JSON.stringify(jsonObjList);

		//Check if list isn't longer than employeesThreshold and after add it or not
		if ($scope.selectedTask.employeesActive + jsonObjList[0].tasksThreshold > $scope.selectedTask.employeesThreshold) {
			//Notif
			return;
		}
		assignEmployeesService.assignEmployees.addJson({},
				jsonObj, function (response) {
			
		});
	}
	
	$scope.deleteTask = function (task) {
		
		var jsonObj = JSON.stringify(task);
		console.log(jsonObj);
		var index = $scope.tasks.indexOf(task);
		$timeout( function(){
			$scope.tasks.splice(index, 1);
		}, 500 );
		deleteServiceTask.deleteTask.deleteJson({},
				jsonObj, function (response) {
			$http.get('/Tasks').then(function(data) {
		        $scope.tasks = data.data;
		    });
		});
	}
	
});

app.controller('LoggedUserTasksController', function ($scope, $http, $timeout){
	
	//Logged user table and notifications controller
	
	$scope.skills = ["None", "Beginner", "Intermediate", "Advanced", "Expert"];
	$scope.autoAssign = false;
	$http.get('/Tasks').then(function(data) {
        $scope.tasks = data.data;
    });
	
	$http.get('/Employees').then(function(data) {
        $scope.employees = data.data;
    });
	
	//Getting logged user
	$http.get('/user').then(function(data) {
		$scope.loggedUser = data.data;
		notification($scope, $timeout, "Hello " + $scope.loggedUser.name, "You have " + $scope.loggedUser.tasks.length + " tasks pending.", "info");
    });
	
	//Find employees assigned to task
	$scope.assignEmployees = function (task) {
		findEmployeesForTask(task, $scope, $scope.selectedTask);
	}
});

app.controller('AddEmployeeController', function ($http, $scope, addEmployeeService, $timeout){
	
	$scope.addNewEmployee = function () {
		var data = $scope.employee;
		var jsonObj = JSON.stringify(data);
		$scope.employees.push(data);
//		$scope.notifMessage = "Successfully registered document " + data.nrReg;

		addEmployeeService.addEmployee.addJson({},
				jsonObj, function (response) {
			notification($scope, $timeout, "Successfully registered " + $scope.employee.name, "info");
		});
	}
});

app.controller('AddTaskController', function ($http, $scope, addTaskService, assignEmployeesService , $timeout){
	
	$scope.addNewTask = function () {

		var nrOfEmployeesNeeded = 0;
		$scope.task.employeesNeeded = {'None': 0, 'Beginner': 0, 'Intermediate': 0, 'Advanced': 0, 'Expert': 0};
		
		if ($scope.code.skill1 == "" || !isNumber($scope.code.nrOfEmployeesNeeded1)) {
			if (parseInt($scope.code.nrOfEmployeesNeeded1) > 0 || angular.isUndefined($scope.code.skill1)) $scope.code.skill1 = -1;
		} else {
			$scope.task.employeesNeeded[$scope.skills[$scope.code.skill1]] = parseInt($scope.code.nrOfEmployeesNeeded1);
			nrOfEmployeesNeeded += parseInt($scope.code.nrOfEmployeesNeeded1);
		}
		if ($scope.code.skill2 == "" || !isNumber($scope.code.nrOfEmployeesNeeded2)) {
			if (parseInt($scope.code.nrOfEmployeesNeeded2) > 0 || angular.isUndefined($scope.code.skill2)) $scope.code.skill2 = -1;
		} else {
			$scope.task.employeesNeeded[$scope.skills[$scope.code.skill2]] = parseInt($scope.code.nrOfEmployeesNeeded2);
			nrOfEmployeesNeeded += parseInt($scope.code.nrOfEmployeesNeeded2);
		}
		if ($scope.code.skill3 == "" || !isNumber($scope.code.nrOfEmployeesNeeded3)) {
			if (parseInt($scope.code.nrOfEmployeesNeeded3) > 0 || angular.isUndefined($scope.code.skill3)) $scope.code.skill3 = -1;
		} else {
			$scope.task.employeesNeeded[$scope.skills[$scope.code.skill3]] = parseInt($scope.code.nrOfEmployeesNeeded3);
			nrOfEmployeesNeeded += parseInt($scope.code.nrOfEmployeesNeeded3);
		}
		if ($scope.code.skill4 == "" || !isNumber($scope.code.nrOfEmployeesNeeded4)) {
			if (parseInt($scope.code.nrOfEmployeesNeeded4) > 0 || angular.isUndefined($scope.code.skill4)) $scope.code.skill4 = -1;
		} else {
			$scope.task.employeesNeeded[$scope.skills[$scope.code.skill4]] = parseInt($scope.code.nrOfEmployeesNeeded4);
			nrOfEmployeesNeeded += parseInt($scope.code.nrOfEmployeesNeeded4);
		}
		if ($scope.code.skill5 == "" || !isNumber($scope.code.nrOfEmployeesNeeded5)) {
			if (parseInt($scope.code.nrOfEmployeesNeeded5) > 0 || angular.isUndefined($scope.code.skill5)) $scope.code.skill5 = -1;
		} else {
			$scope.task.employeesNeeded[$scope.skills[$scope.code.skill5]] = parseInt($scope.code.nrOfEmployeesNeeded5);
			nrOfEmployeesNeeded += parseInt($scope.code.nrOfEmployeesNeeded5);
		}
		
		if (angular.isUndefined($scope.code.nrOfEmployeesNeeded1)) {
			$scope.code.nrOfEmployeesNeeded1 = 0;
		}
		if (angular.isUndefined($scope.code.nrOfEmployeesNeeded2)) {
			$scope.code.nrOfEmployeesNeeded2 = 0;
		}
		if (angular.isUndefined($scope.code.nrOfEmployeesNeeded3)) {
			$scope.code.nrOfEmployeesNeeded3 = 0;
		}
		if (angular.isUndefined($scope.code.nrOfEmployeesNeeded4)) {
			$scope.code.nrOfEmployeesNeeded4 = 0;
		}
		if (angular.isUndefined($scope.code.nrOfEmployeesNeeded5)) {
			$scope.code.nrOfEmployeesNeeded5 = 0;
		}
		
		for (var i = 0; i < 5; i++) {
			if ($scope.task.employeesNeeded[$scope.skills[i]] == 0) {
				delete $scope.task.employeesNeeded[$scope.skills[i]];
			}
		}
		
		var codeToSave = $scope.code.skill1 + " " + $scope.code.nrOfEmployeesNeeded1 + ";" + $scope.code.skill2 + " " + $scope.code.nrOfEmployeesNeeded2 + ";" + 
						$scope.code.skill3 + " " + $scope.code.nrOfEmployeesNeeded3 + ";" + $scope.code.skill4 + " " + $scope.code.nrOfEmployeesNeeded4 + ";" + 
						$scope.code.skill5 + " " + $scope.code.nrOfEmployeesNeeded5 + ";";
		$scope.task.employeesThreshold = nrOfEmployeesNeeded;
		$scope.task.employeesNeededCode = codeToSave;
		var data = $scope.task;
		var jsonObj = JSON.stringify(data);
		
//		$scope.notifMessage = "Successfully registered document " + data.nrReg;
		$scope.tasks.push(data);
		if ($scope.autoAssign) {
			findEmployeesForTask($scope.task, $scope, $scope.selectedTask);
			
			//Set the task and employee list if auto assign was selected
			$scope.task.employeesNeededCode = "";
			$scope.task.employeesActive = nrOfEmployeesNeeded;
			
			var emptyEmployee = {'name': $scope.task.name + "{}" + $scope.task.description, 'tasksThreshold':0, 'skill':0};
			var jsonObjList = [];
			jsonObjList.push(emptyEmployee);
			//Check the number of employees needed and adds them to list
			for (var i = 0; i < $scope.employeeList.length; i++) {
				if ($scope.task.employeesNeeded[$scope.employeeList[i].skill] > 0) {
					$scope.task.employeesNeeded[$scope.employeeList[i].skill]--;
					jsonObjList.push($scope.employeeList[i]);
				} else {
					delete $scope.employeeList[i];
				}
			}
			jsonObj = JSON.stringify(jsonObjList);
			assignEmployeesService.assignEmployees.addJson({},
					jsonObj, function (response) {
			});
		} else {
			addTaskService.addTask.addJson({},
					jsonObj, function (response) {
				notification($scope, $timeout, "Successfully registered " + $scope.task.name, "info");
			});
		}
	}
	function isNumber(n) {
	  return !isNaN(parseFloat(n)) && isFinite(n);
	}
});

function findEmployeesForTask (task, $scope, selectedTask) {
	$scope.employeeList = [];
	$scope.selectedTask = task;
	var alreadyExists = false;
	
	//Check for duplicates and populate the list to show in employee selector
	$scope.employeeList = [];
	for (var i = 0; i < $scope.employees.length; i++) {
		for (var j = 0; j < $scope.employees[i].tasks.length; j++){
			if (task.taskId == $scope.employees[i].tasks[j].taskId) {
				alreadyExists = true;
				break;
			}
		}
		//Conditions to find employees
		if (!alreadyExists && task.employeesNeeded.hasOwnProperty($scope.employees[i].skill) && $scope.employees[i].tasksThreshold > $scope.employees[i].tasks.length) {
			$scope.employeeList.push($scope.employees[i]);
		}
		alreadyExists = false;
	}
	if (task.employeesThreshold <= task.employeesActive) {
		//Notif
		$scope.employeeList = [];
	}
	for (var i = 0; i < $scope.employeeList.length; i++) {
		$scope.employeeList[i].toAdd = false;
	}
}

function notification($scope, $timeout, text, type) {
	$scope.notification.text = text;
	$scope.notification.type = type;
	$("#notifModal").show();
	$timeout(function (){$("#notifModal").hide();}, 4000);
}

function notification($scope, $timeout, title, text, type) {
	$scope.notification.title = title;
	$scope.notification.text = text;
	$scope.notification.type = type;
	$("#notifModal").show();
	$timeout(function (){$("#notifModal").fadeOut();}, 4000);
}