package taskmanager.objects;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import taskmanager.utils.AppHelper;
import taskmanager.utils.AppHelper.Skills;

@Entity(name = "Employees")
public class Employee {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "name")
	private String name;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "role")
	private String role;

	@Column(name = "tasksThreshold")
	private int tasksThreshold;

	@Column(name = "isFree")
	private boolean isFree;
	
	@Column(name = "enabled")
	private boolean enabled;
	
	@Column(name = "skill")
	private AppHelper.Skills skill;
	
	@ManyToMany(fetch = FetchType.EAGER) 
	@JoinTable(
      name="Employees_Tasks",
	  joinColumns=@JoinColumn(name="empId", referencedColumnName="id"),
	  inverseJoinColumns=@JoinColumn(name="taskId", referencedColumnName="taskId"))
	private List<Task> tasks = new ArrayList<Task>();

	public Employee(){
		
	}
	
	public Employee(String name, boolean isFree, AppHelper.Skills skill){
		this.name = name;
		this.isFree = isFree;
		this.skill = skill;
	}
	
	public Employee(String name, String password, String email, String role, int tasksThreshold, boolean isFree, boolean enabled,
			Skills skill, List<Task> tasks) {
		super();
		this.name = name;
		this.password = password;
		this.email = email;
		this.role = role;
		this.tasksThreshold = tasksThreshold;
		this.isFree = isFree;
		this.enabled = enabled;
		this.skill = skill;
		this.tasks = tasks;
	}

	public void addTask(Task task) {
		this.tasks.add(task);
	}
	public Task removeTask(Task task) {
		this.tasks.remove(task);
		return task;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean isFree() {
		return isFree;
	}
	public void setFree(boolean isFree) {
		this.isFree = isFree;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public AppHelper.Skills getSkill() {
		return skill;
	}
	public void setSkill(AppHelper.Skills skill) {
		this.skill = skill;
	}
	public List<Task> getTasks() {
		return tasks;
	}
	public void setTasks(List<Task> task) {
		this.tasks = task;
	}
	public int getTasksThreshold() {
		return tasksThreshold;
	}
	public void setTasksThreshold(int tasksThreshold) {
		this.tasksThreshold = tasksThreshold;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String toString() {
		return "\nEmployee " + this.getId() + "\nName: " + this.getName() + "\nTasks threshold: " + this.getTasksThreshold() + "\nSkill: " + AppHelper.Skills.getSkillLevel(this.getSkill().getSkillLevel())
		+ "\nIs free: " + this.isFree() + "\nTask assigned: " + this.getTasks() + "\n";
	}
}
