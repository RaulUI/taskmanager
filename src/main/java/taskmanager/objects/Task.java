package taskmanager.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import taskmanager.utils.AppHelper;

@Entity(name = "Tasks")
public class Task {

	@Id
	@Column(name = "taskId")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int taskId;
	
	@Column(name = "name")
	private String name;
	@Column(name = "employeesThreshold")
	private int employeesThreshold;
	
	private transient HashMap<AppHelper.Skills, Integer> employeesNeeded = new HashMap<AppHelper.Skills, Integer>();
	
	@Column(name = "employeesNeededCode")
	private String employeesNeededCode;
	
	@Column(name = "employeesActive")
	private int employeesActive;
	@Column(name = "isDone")
	private boolean isDone;
	
	@Column(name = "description")
	private String description;
	
	@ManyToMany(cascade = CascadeType.ALL, mappedBy="tasks")
//	@JsonIgnore
	@JsonProperty(access = Access.WRITE_ONLY)
	private List<Employee> employees = new ArrayList<Employee>();

	public Task () {
		this.employeesActive = 0;
		this.isDone = false;
		this.employeesNeededCode = "";
	}
	
	public Task (String name, int employeesThreshold, String description, String employeesNeededCode) {
		
		this.name = name;
		this.employeesThreshold = employeesThreshold;
		this.employeesNeededCode = employeesNeededCode;
		this.description = description; 
		employeesActive = 0;
		isDone = false;
	}
	
	public void addEmployee(Employee e) {
		this.getEmployees().add(e);
	}
	
	public void addEmployees(List<Employee> e) {
		this.getEmployees().addAll(e);
	}
	
	public int getTaskId() {
		return taskId;
	}
	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getEmployeesThreshold() {
		return employeesThreshold;
	}
	public void setEmployeesThreshold(int employeesThreshold) {
		this.employeesThreshold = employeesThreshold;
	}
	public String getEmployeesNeededCode() {
		return employeesNeededCode;
	}
	public void setEmployeesNeededCode(String employeesNeededCode) {
		this.employeesNeededCode = employeesNeededCode;
	}
	public List<Employee> getEmployees() {
		return employees;
	}
	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
	public HashMap<AppHelper.Skills, Integer> getEmployeesNeeded() {
		return employeesNeeded;
	}
	public void setEmployeesNeeded(HashMap<AppHelper.Skills, Integer> employeesNeeded) {
		this.employeesNeeded = employeesNeeded;
	}
	public int getEmployeesActive() {
		return employeesActive;
	}
	public void setActive(int employeesActive) {
		this.employeesActive = employeesActive;
	}
	public boolean isDone() {
		return isDone;
	}
	public void setDone(boolean isDone) {
		this.isDone = isDone;
	}
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String toString() {
		return "\nTask " + this.getTaskId() + "\nName: " + this.getName() + "\nDescription: " + this.getDescription() + "\nNumber of employees needed: " + this.getEmployeesThreshold()
		+ "\nIs active: " + this.getEmployeesActive()+ "\nIs done: " + this.isDone();
	}
}
