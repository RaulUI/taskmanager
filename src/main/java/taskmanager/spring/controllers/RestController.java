package taskmanager.spring.controllers;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import taskmanager.objects.Employee;
import taskmanager.objects.Task;
import taskmanager.utils.AppHelper;
import taskmanager.utils.DAOHelper;

@org.springframework.web.bind.annotation.RestController@SuppressWarnings("static-access")
public class RestController {
	
	private DAOHelper dao = new DAOHelper();;
	
	@RequestMapping(value = "/user", method = RequestMethod.GET)
    @ResponseBody
    public Employee currentUserName(Principal principal) {
		Employee emp = new Employee();
		try {
			emp = DAOHelper.getEmployeeByEmail(principal.getName());
		} catch (Exception e) {
			
		}
        return emp;
    }
	
	@RequestMapping(value = "/Employees")
    public @ResponseBody List<Object> EmployeesList(Model model) {

		return dao.getAll("Employees");
		
    }
	
	@RequestMapping(value = "/Tasks")
    public @ResponseBody List<Object> TasksList(Model model) {
		
//		Task task = dao.getTaskById(551);
//		
//		task.setEmployeesNeeded(AppHelper.parseEmployeesNeededCode(dao.getTaskById(551).getEmployeesNeededCode()));
		
		for (Object task : dao.getAll("Tasks")) {
			((Task) task).setEmployeesNeeded(AppHelper.parseEmployeesNeededCode(((Task) task).getEmployeesNeededCode()));
		}
		
		return dao.getAll("Tasks");
		
    }
	
	@RequestMapping(value = "/deleteEmployee", method = RequestMethod.POST, consumes = "application/json")
    public String delete(@RequestBody Employee employee) {
		
		dao.delete(employee);
		
		return "Test";
    }
	
	@RequestMapping(value = "/deleteTask", method = RequestMethod.POST, consumes = "application/json")
    public String delete(@RequestBody Task task) {
		System.out.println(task.toString());
		dao.delete(task);
		
		return "Test";
    }
	
	@RequestMapping(value = "/AddNewEmployee", method = RequestMethod.POST, consumes = "application/json")
    public String add(@RequestBody Employee employee) {
		
		Employee e = new Employee(employee.getName(), employee.getPassword(), employee.getEmail(), "USER", 5, true, true, employee.getSkill(), null);
		
		dao.add(e);
		
		return employee.toString();
    }
	
	@RequestMapping(value = "/AddNewTask", method = RequestMethod.POST, consumes = "application/json")
    public String add(@RequestBody Task task) {
		
		Task t = new Task(task.getName(), task.getEmployeesThreshold(), task.getDescription(), task.getEmployeesNeededCode());
		
		dao.add(t);
 
		return task.toString();
    }
	
	@RequestMapping(value = "/EditEmployee", method = RequestMethod.POST, consumes = "application/json")
    public String update(@RequestBody Employee employee) {
		System.out.println(employee);
		dao.update(employee);
		
		return employee.toString();
    }
	
//	@RequestMapping(value = "/EditTask", method = RequestMethod.POST, consumes = "application/json")
//    public String update(@RequestBody Task task) {
//		
//		Task t = new Task(task.getName(), task.getEmployeesThreshold(), task.getDescription(), task.getEmployeesNeededCode());
//		
//		dao.add(t);
//		
//		return task.toString();
//    }
	
	@RequestMapping(value = "/AssignEmployees", method = RequestMethod.POST, consumes = "application/json")
    public String assignEmployees(@RequestBody List<Employee> employees) {
		
		Task task;
		//Add task from an employee list adding the number of employees from employee's treshold
		try {
			task = dao.getTaskById(Integer.parseInt(employees.get(0).getName()));
		} catch (Exception e) {
			task = new Task();
			task.setName(employees.get(0).getName().substring(0, employees.get(0).getName().indexOf("{")));
			task.setDescription(employees.get(0).getName().substring(employees.get(0).getName().indexOf("}") + 1));
			task.setEmployeesNeededCode("");
			task.setEmployeesThreshold(employees.size()-1);
			task.setActive(employees.size()-1);
			dao.add(task);
		}
		
		task .setActive(task.getEmployeesActive() + employees.get(0).getTasksThreshold());
		employees.remove(0);
		List<Employee> employeesList = new ArrayList<Employee>();
		
		for (Employee e : employees) {
			if (!task.getEmployeesNeededCode().isEmpty())
				AppHelper.removeEmployeeFromCode(e, task);
			employeesList.add((Employee) DAOHelper.getEmployeeById(e.getId()));
		}
		
		DAOHelper.addTaskToEmployees(task, employeesList);
		return employees.toString();
    }
	
	@RequestMapping(value = "/AssignTasks", method = RequestMethod.POST, consumes = "application/json")
    public String assignTasks(@RequestBody List<Task> tasks) {
		
		Employee e = dao.getEmployeeById(Integer.parseInt(tasks.get(0).getName()));
		tasks.remove(0);
		System.out.println(e.toString());

		for (Task t : tasks) {
			t = DAOHelper.getTaskById(t.getTaskId());
			t.setActive(t.getEmployeesActive() + 1);
			AppHelper.removeEmployeeFromCode(e, t);
			System.out.println(t.toString());
		}
		
		DAOHelper.addTasksToEmployee(e, tasks);
		
		return tasks.toString();
    }
}
