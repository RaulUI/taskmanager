package taskmanager.spring.security;


import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import taskmanager.utils.DAOHelper;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Bean
	  public DataSource appDataSource() {
	      DriverManagerDataSource ds = new DriverManagerDataSource();
	      ds.setDriverClassName("org.sqlite.JDBC");
	      ds.setUrl("jdbc:sqlite:task_manager.db");
	      ds.setUsername("");
	      ds.setPassword("");
	      return ds;
	  }
	
    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	
    	//For logout
    	http.csrf().disable();
    	http.headers().frameOptions().disable();
    	
    	http
        .authorizeRequests()
            .antMatchers("/", "/login").permitAll()
            .anyRequest().authenticated()
            .and()
        .formLogin()
            .loginPage("/login")
            .successForwardUrl("/")
            .failureForwardUrl("/")
            .permitAll()
            .and()
        .logout().logoutSuccessUrl("/")
            .permitAll()
            .and();
    	
    }
    
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
            .inMemoryAuthentication()
                .withUser("user").password("password").roles("USER");
        auth.jdbcAuthentication().dataSource(appDataSource())
		.usersByUsernameQuery(
			"select email, password, enabled from Employees where email=?")
		.authoritiesByUsernameQuery(
			"select email, role from Employees where email=?");
    }
}