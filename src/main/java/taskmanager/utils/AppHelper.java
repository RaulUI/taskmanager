package taskmanager.utils;

import java.util.HashMap;

import taskmanager.objects.Employee;
import taskmanager.objects.Task;

public class AppHelper {
	
	public enum Skills {
		
		None(0),
		Beginner(1),
		Intermediate(2),
		Advanced(3),
		Expert(4);
		
		private int skillLevel;
		
		Skills(int skillLevel){
			this.setSkillLevel(skillLevel);
		}

		public int getSkillLevel() {
			return skillLevel;
		}
		
		public static Skills getSkillLevel(int skillLevel) {
			return getSkillFromLevel(skillLevel);
		}

		public void setSkillLevel(int skillLevel) {
			this.skillLevel = skillLevel;
		}
		
		public static Skills getSkillFromLevel(int skillLevel) {
			switch(skillLevel) {
			case 0: return None;
			case 1: return Beginner;
			case 2: return Intermediate;
			case 3: return Advanced;
			case 4: return Expert;
			default: return None;
			}
			
		}
	}
	
	public static HashMap<AppHelper.Skills, Integer> parseEmployeesNeededCode (String employeesNeededCode) {
		
		int skillLevel = 0;
		Integer nr = 0;
		HashMap<AppHelper.Skills, Integer> employeesNeeded = new HashMap<AppHelper.Skills, Integer>();
		
		//Parse employees needed code
		try {
			while (!employeesNeededCode.isEmpty()) {
				if (employeesNeededCode.substring(0, 1).equals("-")) {
					employeesNeededCode = employeesNeededCode.substring(employeesNeededCode.indexOf(";") + 1);
				} else {
					skillLevel = Integer.parseInt(employeesNeededCode.substring(0, 1));
					employeesNeededCode = employeesNeededCode.substring(2);
					nr = Integer.parseInt(employeesNeededCode.substring(0, employeesNeededCode.indexOf(";")));
					employeesNeededCode = employeesNeededCode.substring(employeesNeededCode.indexOf(";") + 1);
					
	//				System.out.println(Skills.getSkillLevel(skillLevel) + " - " + nr);
					employeesNeeded.put(Skills.getSkillLevel(skillLevel), nr);
				}
			}
		} catch (Exception e) {
			System.out.println("Wrong Employees Needed Code format!\nIt should look like 'skill_level employes_nr;skill_level employes_nr;'!");
		}
		
		return employeesNeeded;
		
	}
	
	public static void removeEmployeeFromCode(Employee e, Task t) {
		
		String codeEnd , codeStart, code = t.getEmployeesNeededCode();
		
		codeStart = code.substring(0, code.indexOf(Integer.toString(e.getSkill().getSkillLevel()) + " "));
		codeEnd = code.substring(code.indexOf( e.getSkill().getSkillLevel() + " "));
		
		int eNr = Integer.parseInt(codeEnd.substring(2, codeEnd.indexOf(";"))) - 1;
		if (eNr <= 0) {
			codeEnd = codeEnd.substring(codeEnd.indexOf(";")+1);
		} else {
			codeEnd = codeEnd.replaceFirst(Integer.toString(eNr+1) + ";", Integer.toString(eNr) + ";");
		}
		
		t.setEmployeesNeededCode(codeStart + codeEnd);
	}
	
}
