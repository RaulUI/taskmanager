package taskmanager.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import taskmanager.objects.Employee;
import taskmanager.objects.Task;
import taskmanager.utils.AppHelper.Skills;

public class DAOHelper {

	static EntityManagerFactory factory = Persistence.createEntityManagerFactory("taskmanager");
	
	@PersistenceContext
	static EntityManager em = factory.createEntityManager();
	
//	public static DataSource getDataSourceFromHibernateEntityManager() {
//	   EntityManagerFactoryInfo info = (EntityManagerFactoryInfo) em.getEntityManagerFactory();
//	   return info.getDataSource();
//	}
	
	public static Task getTaskById(int taskId) {
		
		Task task = (Task) em.createQuery("SELECT t FROM Tasks t WHERE t.taskId = " + taskId).getSingleResult();
		return  task;
	
	}
	
	public static Employee getEmployeeById(int empId) {
		
		Employee e = (Employee) em.createQuery("SELECT t FROM Employees t WHERE t.id = " + empId).getSingleResult();
		return  e;
	
	}
	
	public static Employee getEmployeeByEmail(String email) {
		
		Employee e = (Employee) em.createQuery("SELECT t FROM Employees t WHERE t.email = " + "\"" + email + "\"").getSingleResult();
		return  e;
	
	}

	@SuppressWarnings("unchecked")
	public static List<Employee> searchForEmployees(String employeesNeededCode) {
		
		HashMap<AppHelper.Skills, Integer> employeesNeeded = AppHelper.parseEmployeesNeededCode(employeesNeededCode);
		
		//Prepare skill level string for query
		Skills [] skills = new Skills[5];
		
		int i = 0;
		for (Skills skill : employeesNeeded.keySet()) {
			skills[i] = skill;
			i++;
		}
		List<Skills> skillLevelToSearch = Arrays.asList(skills);
		
		String qlString = "select e from Employees e where e.skill IN :skillLevelToSearch"; 
		Query q = em.createQuery(qlString, Employee.class);

		q.setParameter("skillLevelToSearch", skillLevelToSearch);
		
		return q.getResultList();
		
	}
	
	/**
	 * Adds a list of {@code Task}s to an {@code Employee}.
	 *
	 * @param  task the {@code Task} to be added to each {@code Employee}
	 * @param  employees the {@code Employee} list to get the task
	 */
	public static  void addTasksToEmployee(Employee employee, List<Task> tasks) {
		em.getTransaction().begin();

		for (Task t : tasks) {
			employee.addTask(t);
		}
		
		System.out.println("Success!");
		em.getTransaction().commit();
	}
	
	/**
	 * Adds a {@code Task} to a list of {@code Employee}s.
	 *
	 * @param  task the {@code Task} to be added to each {@code Employee}
	 * @param  employees the {@code Employee} list to get the task
	 */
	public static  void addTaskToEmployees(Task task, List<Employee> employees) {
		em.getTransaction().begin();
		for (Employee employee : employees) {
//			task.addEmployee(employee);
//			employee.addTask(task);
			employee.getTasks().add(task);
		}
		em.getTransaction().commit();
		System.out.println("Success!");
	}
	
	/**
	 * Gets all elements from Database.
	 *
	 * @param  tableName the name of the table to be retrieved from Database
	 */
	@SuppressWarnings("unchecked")
	public static List<Object> getAll(String tableName) {
		return em.createQuery("SELECT e FROM " + tableName + " e").getResultList();
	}
	
	/**
	 * Adds an {@code Object} to (Database).
	 *
	 * @param  obj the {@code Object} to be added to database
	 */
	public static void add(Object obj) {
		em.getTransaction().begin();
		em.persist(obj);
		em.flush();
		em.getTransaction().commit();

	}

	/**
	 * Updates an {@code Object}.
	 *
	 * @param  obj the {@code Object} to be added to database
	 */
	public static void update(Employee employee) {
		
		em.getTransaction().begin();
		getEmployeeById(employee.getId()).setName(employee.getName());
		getEmployeeById(employee.getId()).setSkill(employee.getSkill());
		em.getTransaction().commit();

	}
	
	/**
	 * Deletes an {@code Object} from (Database).
	 *
	 * @param  obj the {@code Object} to be deleted from database
	 */
	public static void delete(Object obj) {
		em.getTransaction().begin();
		if (!em.contains(obj)) {
			obj = em.merge(obj);
		}
		
		em.remove(obj);
		em.getTransaction().commit();
	}

}
