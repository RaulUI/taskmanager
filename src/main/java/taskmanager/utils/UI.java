//package taskmanager.utils;
//
//import java.util.InputMismatchException;
//import java.util.Scanner;
//
//import javax.persistence.NoResultException;
//
//import taskmanager.objects.Employee;
//import taskmanager.objects.Task;
//import taskmanager.utils.AppHelper.Skills;
//
//public class UI {
//	
//	private Scanner userInput = new Scanner(System.in);
//	
//	public UI() {
//		
//		menu();
//		
//		switch(userInput.nextInt()) {
//
//		case 1: addEmployee();break;
//		case 2: showAllEmployees();break;
//		case 3: addTask();break;
//		case 4: activateTask();break;
//		case 5: showAllTasks();break;
//		
//		default: System.out.println("Invalid menu item selection.");
//		}
//		
//	}
//
//	private void menu() {
//		System.out.println("1. Add employee");
//		System.out.println("2. Show all employees");
//		System.out.println("3. Add task");
//		System.out.println("4. Activate task");
//		System.out.println("5. Show all tasks");
//	}
//	
//	
//	private void showAllEmployees() {
//		System.out.println("\n~~~ Employees list BEGGINING ~~~\n");
////		for(Employee employee : DAOHelper.getEmployees()) {
////			System.out.println(employee.toString());
////		}
////		System.out.println("\n~~~ Employees list END ~~~\n");
////		new UI();
//	}
//	
//	private void showAllTasks() {
//		System.out.println("\n~~~ Tasks list BEGGINING ~~~\n");
//		for(Task task : DAOHelper.getTasks()) {
//			System.out.println(task.toString());
//		}
//		System.out.println("\n~~~ Tasks list END ~~~\n");
//		new UI();
//	}
//	
//	private void addEmployee() {
//		
//		Employee employee = new Employee();
//		System.out.println("Add employee selected.\n");
//		
//		System.out.println("Type employee's name: ");
//		employee.setName(userInput.next());
//		
//		System.out.println("\nType 1 if employee is free for a new task, 0 otherwise: ");
//		employee.setFree(Boolean.parseBoolean(userInput.next()));
//		
//		System.out.println("\nType employee's skill level from 0 to 4: ");
//		employee.setSkill(AppHelper.Skills.getSkillFromLevel(userInput.nextInt()));
//		
//		try {
//			DAOHelper.addEmployee(employee);
//			System.out.println("User " + employee.getName() + " successfully added to database!");
//		} catch(Exception e) {
//			System.out.println("Could not add user " + employee.getName() + " to database!");
//		}
//		new UI();
//	}
//	
//	private void addTask() {
//		Task task = new Task ();
//		System.out.println("Add task selected.\n");
//		
//		System.out.println("Type task's name: ");
//		task.setName(userInput.next());
//		
//		System.out.println("\nType task's description: ");
//		task.setDescription(userInput.next());
//		
//		System.out.println("\nType task's number of employees needed: ");
//		task.setNrOfEmployeesNeeded(userInput.nextInt());
//
//		try {
////			DAOHelper.addTask(task);
//			System.out.println("Task " + task.getName() + " successfully added to database!");
//		} catch(Exception e) {
//			System.out.println("Could not add task " + task.getName() + " to database!");
//		}
//		new UI();
//	}
//	
//	private void activateTask() {
//		
//		System.out.println("Activate task selected.\n");
//		
//		System.out.println("Type task's id: ");
//		int idToFind = 0;
//		Task task = null;
//		String employeesCode = "";
//		
//		try {
//			idToFind = userInput.nextInt();
//			userInput.nextLine();
//			task = DAOHelper.getTaskById(idToFind);
//		} catch (NoResultException e) {
//			System.out.println("Task " + idToFind + " doesn't exist!");
//			new UI();
//		} catch (InputMismatchException e) {
//			System.out.println("Task id must be a number!");
//			new UI();
//		} catch (Exception e) {
//			System.out.println("Task " + idToFind + " couldn't be get from db!");
//			new UI();
//		}
//		
//		System.out.println(task.toString() + "\nTask " + idToFind + " Selected\n");
//		System.out.println("Type task's Employees Needed Code: \n(it should look like 'skill_level employes_nr;skill_level employes_nr;')");
//		employeesCode = userInput.nextLine();
//		
//		AppHelper.parseEmployeesNeededCode(employeesCode);
//		
//		int sumOfEmployeers = 0;
//		for (Skills key : AppHelper.parseEmployeesNeededCode(employeesCode).keySet()) {
//			sumOfEmployeers += AppHelper.parseEmployeesNeededCode(employeesCode).get(key);
//		}
//		if (task.getNrOfEmployeesNeeded() != sumOfEmployeers) {
//			System.out.println("Number must be equal task's Employees Number");
//			new UI();
//		}
//		task.setEmployeesNeededCode(employeesCode);
////		DAOHelper.updateTaskCode(task);
//		
//		System.out.println(DAOHelper.searchForEmployees(employeesCode));
//		
//		new UI();
//	}
//}